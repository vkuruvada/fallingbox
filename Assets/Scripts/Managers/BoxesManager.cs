﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BoxesManager : MonoBehaviour {

	public static BoxesManager Instance;

    public GameObject normalBox;
    public GameObject magicBox;
    public GameObject ironBox;
	public GameObject superBox;

    public List<Sprite> sprites;
    public Transform spawnLocation;
	public bool createNewBox = false;

	private Vector2 screenSize;
	private Vector3 cameraPos;
	private float rayBuffer = 0f;

    private GameObject currentBox;
    private List<GameObject> boxes;
    private int initialBoxesCount = 10;
    private float spawnDelay = 0.5f;
	private System.DateTime lastSpawnTime = System.DateTime.Now;

    public delegate void InitialCubesDropped();
    public static event InitialCubesDropped cubesDropped;

	void Awake() {

		if (Instance != null && Instance != this) {
			Destroy (gameObject);
		} else  {
			Instance = this;
		}
	}

    // Use this for initialization
    void Start () {

        boxes = new List<GameObject>();
        IEnumerator coroutine = CreateInitialBoxex(spawnDelay);
        StartCoroutine(coroutine);

		cameraPos = Camera.main.transform.position;
		screenSize.x = Vector2.Distance (Camera.main.ScreenToWorldPoint(new Vector2(0,0)),Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
		screenSize.y = Vector2.Distance (Camera.main.ScreenToWorldPoint(new Vector2(0,0)),Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;
		transform.position = new Vector3 (-screenSize.x+rayBuffer, -screenSize.y + (screenSize.y/3), transform.position.z);

	}

    private void OnEnable()
    {
        Box.removeBox += removeAndDestroyBox;
    }

    private void OnDisable()
    {
        Box.removeBox -= removeAndDestroyBox;
    }

    // Update is called once per frame
    void FixedUpdate () {
		Vector3 forward = transform.TransformDirection (Vector2.right);
		Debug.DrawRay (transform.position, forward * screenSize.x * 2, Color.green);

        if (currentBox != null && 
			!currentBox.GetComponent<Rigidbody2D> ().isKinematic && 
			(currentBox.GetComponent<Rigidbody2D> ().IsSleeping() || currentBox.GetComponent<Box> ().timeSinceSpawn > 3))
        {
            currentBox.GetComponent<Box>().isCurrent = false;
			if (currentBox.GetComponent<Box> ().didHitSameColor && currentBox.GetComponent<Box>().hitCount <= 0) {
                removeAndDestroyBox(currentBox);
			}

            if (!checkGameEnd ()) {
				createNewBox = true;
            }
        }

        if (createNewBox)
        {
            createDroppableBox();
        }
    }

	bool checkGameEnd () {
		Vector3 forward = transform.TransformDirection (Vector2.right);
		RaycastHit2D hit = Physics2D.Raycast (transform.position, forward, screenSize.x*2);
		if (hit.collider != null && hit.collider.name.IndexOf("boundry") == -1 ) {
			GameManager.Instance.showGameOver ();
			return true;
		}
		return false;

	}

    IEnumerator CreateInitialBoxex(float time)
    {
        int i = initialBoxesCount;
        while (i >=0)
        {
            yield return new WaitForSeconds(time);
            GameObject newBox = createNormalBox();
            setupBox(newBox);
            i--;
        }

        if (cubesDropped != null)
        {
            cubesDropped();
			yield return new WaitForSeconds(time);
            createDroppableBox();
			GameManager.Instance.gameStarted = true;
        }
    }

    private void setupBox(GameObject newBox)
    {
        float scale = Random.Range(0.25f, 0.40f);
        newBox.transform.localScale =new Vector3(scale, scale, 0);
		newBox.GetComponent<BoxCollider2D> ().edgeRadius = scale * 0.5f;
		newBox.GetComponent<Rigidbody2D> ().mass = scale * 50;
        boxes.Add(newBox);
    }

	private GameObject CreateBox() {
        GameObject box;
		int itemIndex = Random.Range (0, 100);
		if (itemIndex >=30) {
			box = Instantiate(superBox, spawnLocation.position, Quaternion.Euler(0, 0, Random.Range(0, 45)));
		} else if (itemIndex >=90) {
			box = Instantiate(magicBox, spawnLocation.position, Quaternion.Euler(0, 0, Random.Range(0, 45)));
		}  else if (itemIndex >= 80) {
			box = Instantiate(ironBox, spawnLocation.position, Quaternion.Euler(0, 0, Random.Range(0, 45)));
		} else {
            box = createNormalBox();
        }

        return box;
	}

    private GameObject createNormalBox()
    {
        GameObject box = Instantiate(normalBox, spawnLocation.position, Quaternion.Euler(0, 0, Random.Range(0, 45)));
        int index = Random.Range(0, 9);
        box.GetComponent<Box>().setSpriteIndex(index);
        box.GetComponent<SpriteRenderer>().sprite = sprites[index];
        return box;
    }

    public void removeAndDestroyBox(GameObject obj)
    {
        boxes.Remove(obj);
        Destroy(obj);
		GameManager.Instance.score += 10;
    }

    public void createDroppableBox()
    {

        currentBox = CreateBox();
        setupBox(currentBox);
        currentBox.GetComponent<Box>().isCurrent = true;
		currentBox.GetComponent<Box> ().isFalling = false;
		currentBox.GetComponent<Rigidbody2D> ().isKinematic = true;
        createNewBox = false;
    }


    public void removeBoxesOfType(int index)
    {
        List<GameObject> removeList = new List<GameObject>();
        foreach(GameObject box in boxes)
        {
            if (box.GetComponent<Box>().getSpriteIndex() == index)
            {
                Destroy(box, 0.1f);
                removeList.Add(box);
            }
        };

        removeList.ForEach((box) =>
        {
            boxes.Remove(box);
        });
    }
}
