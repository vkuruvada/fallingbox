﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager Instance; 
	public GameObject scoreObj;
	public GameObject topScore;
	public GameObject levelObj;
	public Animator myAnim;
	public Animator pauseAnim;

	public Transform top;
	public Transform bottom;
	public Transform left;
	public Transform right;


	private Vector2 screenSize;
	private Vector3 cameraPos;
	private const string TOP_SCORE = "topScore"; 
	private float collisionDiff = 0.65f;


	private int levelChangeScore = 100;
	public bool gameStarted = false;

	public int level = 1;
	private int _score = 0;
	public int score {
		get {
			return _score;
		}
		set {
			_score = value;

			float currentLevel = (float) _score / levelChangeScore;
			checkLevel(currentLevel);
		}
	}

	void Awake() {
	
		Application.targetFrameRate = 90;
        if (Instance != null && Instance != this) {
			Destroy (gameObject);
		} else {
			Instance = this;
		}
	}

	// Use this for initialization
	void Start () {
		scoreObj = GameObject.Find ("ScoreText");
		topScore = GameObject.Find ("TopScore");
		levelObj = GameObject.Find ("Level");

		topScore.GetComponent<Text> ().text = "Top:" + PlayerPrefs.GetInt (TOP_SCORE, 0);;
		positionsBoundries ();
	}
		
    private void OnEnable()
    {
        BoxesManager.cubesDropped += beginGame;
    }

    private void OnDisable()
    {
        BoxesManager.cubesDropped -= beginGame;
    }

    // Update is called once per frame
    void Update () {
		scoreObj.GetComponent<Text> ().text = score + "";
	}

	private void positionsBoundries() {

		cameraPos = Camera.main.transform.position;
		screenSize.x = Vector2.Distance (Camera.main.ScreenToWorldPoint(new Vector2(0,0)),Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
		screenSize.y = Vector2.Distance (Camera.main.ScreenToWorldPoint(new Vector2(0,0)),Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;

		bottom.position = new Vector3 (top.position.x, -screenSize.y, transform.position.z);
		top.position = new Vector3 (top.position.x, screenSize.y-collisionDiff, transform.position.z);
		right.position = new Vector3 (screenSize.x, right.position.y, transform.position.z);
		left.position = new Vector3 (-screenSize.x, left.position.y, transform.position.z);
	}

    void beginGame()
    {

    }

	void checkLevel(float scoreBasedLevel) {
		if (Mathf.Ceil(scoreBasedLevel) > level) {
			level = (int) Mathf.Ceil(scoreBasedLevel);
			levelObj.GetComponent<Text> ().text = "Level: "+ level;
		}
	}

	public void showGameOver() {

		int topScore = PlayerPrefs.GetInt (TOP_SCORE, 0);
		if (score > topScore) {
			PlayerPrefs.SetInt (TOP_SCORE, score);
		}
			
		myAnim.SetBool ("gameOver", true);
	}

	public void restart() {
		myAnim.SetBool ("gameOver", false);
		IEnumerator coroutine = reloadAfter(0.3f);
		StartCoroutine(coroutine);
	}

	public void pauseRestart() {
		pauseAnim.SetBool ("showPause", false);
		IEnumerator coroutine = reloadAfter(0.3f);
		StartCoroutine(coroutine);
	}

	public void menu() {
		SceneManager.LoadScene ("Menu");
	}

	public void pause() {
		pauseAnim.SetBool ("showPause", true);
	}

	public void hidePause() {
		pauseAnim.SetBool ("showPause", false); 
	}

	IEnumerator reloadAfter(float time) {
		yield return new WaitForSeconds(time);
		SceneManager.LoadScene ("Game");
	}
}
