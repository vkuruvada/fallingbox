﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicBox : NormalBox
{

    protected override void Awake()
    {
        base.Awake();
    }

    // Use this for initialization
    protected override void Start () {
        base.Start();
	}

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (!isCurrent)
        {
            StartCoroutine(coroutine);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        this.hitCount--;
        didHitSameColor = true;
    }

    public override void hitCountUpdated()
    {
    }
}
