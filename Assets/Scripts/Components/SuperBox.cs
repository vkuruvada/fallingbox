﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperBox : Box {

	public Animator superAnim;
    private const string DIALOG_ITEMS_SORT_NAME = "Dialog items";
    private bool didHit = false;

	protected override void Awake ()
	{
		base.Awake ();
		superAnim = GameObject.Find ("superpanel").GetComponent<Animator> ();
	
	}

	// Use this for initialization
	protected override void Start ()
	{
		base.Start ();
		superAnim.SetBool ("show", true);
	}

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            if (!didHit && hit.collider != null && hit.collider.gameObject.GetComponent<SpriteRenderer>().sortingLayerName.Equals(DIALOG_ITEMS_SORT_NAME))
            {
                Box boxScript = gameObject.GetComponent<Box>();
                didHit = true;
                boxScript.didHitSameColor = true;
                boxScript.hitCount = 0;
                gameObject.GetComponent<Rigidbody2D>().isKinematic = false;

                superAnim.SetBool("show", false);
                int index = -1;
                if (int.TryParse(hit.collider.name, out index))
                {
                    removeItemsWithSprite(int.Parse(hit.collider.name));
                } else
                {
                    print("Hellllllo");
                }
            }

        }
    }

    void removeItemsWithSprite(int index)
    {
        BoxesManager.Instance.removeBoxesOfType(index);
    }

    public override void hitCountUpdated()
    {
    }

}
