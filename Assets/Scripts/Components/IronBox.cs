﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IronBox : NormalBox
{

    protected override void Awake()
    {
        base.Awake();
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "MagicBox")
        {
            /* if (!collidedIds.Contains (collision.gameObject.GetInstanceID ())) {
				collidedIds.Add (collision.gameObject.GetInstanceID ());
			} */
            this.hitCount = 0;
            return;
        }
    }

    public override void hitCountUpdated()
    {
        StartCoroutine(coroutine);
    }
}
