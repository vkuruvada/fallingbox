﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class Box : MonoBehaviour
{

    protected int spriteIndex = -1;
    protected BoxCollider2D myCollider;
    protected System.DateTime _lastSpawn = System.DateTime.Now;

    private int maxHitCount = 0;
    private int _hitCount = 0;
	private HashSet<float> collidedIds = new HashSet<float> ();
	private Animator destroyAnim;

    public delegate void RemoveBox(GameObject obj);
    public static event RemoveBox removeBox;
    public bool isFalling = false;
    public Rigidbody2D myBody;
    public bool isCurrent = false;
	public bool didHitSameColor = false;
	public IEnumerator coroutine;
    
	public int hitCount {
        get
        {
            return _hitCount;
        }
        set
        {
            _hitCount = value;
            hitCountUpdated();
        }
    }

	public int timeSinceSpawn {
		get {
			if (myBody.isKinematic)
				return 0;
			return (System.DateTime.Now - _lastSpawn).Seconds;
		}
	}

    // Use this for initialization
    protected virtual void Start () {
        if (GameManager.Instance)
        {
			_hitCount = (int) Random.Range(1, Mathf.Ceil(GameManager.Instance.level/5.0f) * 5);
            isFalling = true;
        }

    }

    protected virtual void Awake()
    {
        myBody = GetComponent<Rigidbody2D>();
		myCollider = GetComponent<BoxCollider2D> ();
		destroyAnim = GetComponent<Animator> ();
		coroutine = destroyCoroutine(0.2f);
    }

    // Update is called once per frame
    protected virtual void FixedUpdate () {
		
        if (isFalling)
        {
            isFalling = !myBody.IsSleeping();
        }
	}

	void OnTriggerEnter2D(Collider2D other) {

		if (other.tag == "MaxHeight") {
			SceneManager.LoadScene ("Game");
		}
	}

    public void setSpriteIndex(int i)
    {
        spriteIndex = i;
    }

    public int getSpriteIndex()
    {
        return spriteIndex;
    }

    public virtual void hitCountUpdated()
    {
        transform.GetChild(0).GetComponent<TextMesh>().text = hitCount >=0? hitCount+"": "0";
        if (hitCount <= maxHitCount && !isCurrent)
        {
			StartCoroutine(coroutine);
        }
    }

	protected IEnumerator destroyCoroutine(float delay) {

		destroyAnim.SetBool ("destroy", true);
		yield return new WaitForSeconds(delay);
		removeBox(gameObject);
	}
}
