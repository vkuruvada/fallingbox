﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class NormalBox : Box, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    private float offsetX, offsetY = 0;
    private Vector2 originalColliderSize;
    private Vector2 screenSize;
    private Vector3 cameraPos;

    public SpriteRenderer rendererComponent;

    protected override void Awake()
    {
        base.Awake();
        rendererComponent = GetComponent<SpriteRenderer>();
    }

    // Use this for initialization
    protected override void Start () {

        base.Start();

        if (tag == "NormalBox")
        {
            transform.GetChild(0).GetComponent<TextMesh>().text = hitCount + "";
        }

        cameraPos = Camera.main.transform.position;
        screenSize.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
        if (isCurrent)
        {
            originalColliderSize = myCollider.size;
            myCollider.size = new Vector2(myCollider.size.x * 1.5f, myCollider.size.y * 1.5f);
        }
    }

    public void OnBeginDrag(PointerEventData data)
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(data.position);
        offsetX = transform.position.x - mousePos.x;
        offsetY = transform.position.y - mousePos.y;

    }

    public void OnDrag(PointerEventData data)
    {
        if (isCurrent && myBody.isKinematic)
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(data.position);
            float val = (float)Mathf.Clamp(position.x + offsetX, -screenSize.x + rendererComponent.bounds.size.x * 0.5f, screenSize.x - rendererComponent.bounds.size.x * 0.5f);
            transform.position = new Vector3(val, transform.position.y, transform.position.z);
        }
    }

    public void OnEndDrag(PointerEventData data)
    {
        myBody.isKinematic = false;
        myCollider.size = originalColliderSize;
        _lastSpawn = System.DateTime.Now;
    }
		

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Box boxScript = collision.gameObject.GetComponent<NormalBox>();
        if (!boxScript)
            return;

        int collidedObjectIndex = boxScript.getSpriteIndex();
        if ( this.spriteIndex == collidedObjectIndex || collision.gameObject.tag == "MagicBox")
        {
            /*
			if (!collidedIds.Contains (collision.gameObject.GetInstanceID ())) {
				collidedIds.Add (collision.gameObject.GetInstanceID ());
				this.hitCount = collision.gameObject.tag == "MagicBox" ? 0: this.hitCount-1;
			}
			*/
            this.hitCount--;
            didHitSameColor = true;
        }
        else if (this.tag == "MagicBox")
        {
            /*
			if (!collidedIds.Contains (collision.gameObject.GetInstanceID ())) {
				collidedIds.Add (collision.gameObject.GetInstanceID ());
				this.hitCount--;
			}
			*/
            this.hitCount--;
            didHitSameColor = true;
        }
    }

}
